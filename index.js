const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout
});
/**
 * Stretch goal: Limit by refinement capacity and number of engineers, e.g., refinerRoulette(2) = Assign all ICs 2 issues
 * Stretch goal: verify that ICs have issues assigned based on their capacity, e.g., Aish may not have an issue to refine but others do.
 * Stretch goal: dry-run option
 * Stretch goal: output log of issues affected and assignees
 * */

// const disciplines = ["backend", "frontend", "UX"];
const disciplines = ["backend", "frontend"];
const teamMembers = {
  backend: ["asubramanian1", "mwoolf", "tancnle"],
  UX: ["aregnery"],
  frontend: ["jiaan", "rob.hunt"]
};
const TOKEN = "TOKEN";

// * Query all issues that fit criteria, check discipline labels, select random assignee to refine based on that.
// * - Issues with ~"Next Up" and ~"group::x" and does not have a milestone will be selected to assign refiners.
const request = require("request");
const query = "https://gitlab.com/api/v4/projects/278964/issues?scope=all&labels=devops%3A%3Amanage,group%3A%3Acompliance,Next%20Up&milestone=None&private_token=" + TOKEN;

let selectRandomAssignee = function(discipline, issueAssignees) {
  let disciplineGroup = teamMembers[discipline];

  if (issueAssignees.includes(disciplineGroup)) {
    return;
  }

  let randomNumber = Math.floor(Math.random() * disciplineGroup.length);

  return disciplineGroup[randomNumber];
};

glRequest = request(query, function(err, res, body) {
  let response = JSON.parse(body);

  response.forEach(issue => {
    let { title, iid, labels, assignees } = issue;

    disciplines.forEach(discipline => {
      if (labels.includes(discipline)) {
        let assignee = selectRandomAssignee(discipline, assignees);
        console.log(`Assigning ${assignee} to "${title}" (#${iid}) because ${discipline} was detected.`);
      }
    });
  });

  // Randomly pick team members based on above disciplines

  readline.question("Proceed with assignments? (Y/N)", response => {
    if (response && response === "Y") {
      console.log("Doing the thing!");
      // Do the assignments
      //
    }

    readline.close();
    process.exit(1);
  });
});
